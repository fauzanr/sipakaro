<!-- Begin Page Content -->
<div class="container-fluid">

<!-- Page Heading -->

<h1 class="h3 mb-4 text-gray-800" style="float: right;">Pengisian Kuesioner Skala</h1>
<h1 class="h3 mb-4 text-gray-800">Skala</h1>
<h1 class="h4 mb-4 text-gray-800">Skala Sapi - RPH</h1>
<br>

<div class="table-responsive">
    <table class="table table-hover">
        <thead class="thead-dark ">
            <tr>
                <th scope="col"></th>
                <th scope="col">Indikator</th>
                <th scope="col">Nilai Skala</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <th scope="row">1</th>
                <td><label for=""></label>PE1</td>
                <td><input type="text" class="form-control" value="Rp21.769.422"></td>
            </tr>
            <tr>
                <th scope="row">2</th>
                <td><label for=""></label>PE2</td>
                <td><input type="text" class="form-control" value="Rp21.769.422"></td>
            </tr>
            <tr>
                <th scope="row">1</th>
                <td><label for=""></label>PE3</td>
                <td><input type="text" class="form-control" value="Rp21.769.422"></td>
            </tr>
            <tr>
                <th scope="row">2</th>
                <td><label for=""></label>PE4</td>
                <td><input type="text" class="form-control" value="Rp21.769.422"></>
            </tr>
        </tbody>
    </table>
</div>

<br>
<h2 class="h4 mb-4 text-gray-800 d-flex justify-content-center">Kriteria Skor Indikator</h2>

<table class="table table-hover">
    <thead class="thead-dark ">
        <tr>
            <th scope="col">Indikator</th>
            <th scope="col">1</th>
            <th scope="col">2</th>
            <th scope="col">3</th>
            <th scope="col">4</th>
            <th scope="col">5</th>
            <th scope="col">6</th>
            <th scope="col">Skor Indikator</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><label for=""></label>PE1</td>
            <td>Rp 17.163.215,1 - Rp 27.065.497,96</td>
            <td>Rp 27.065.497,96 - Rp 36.967.760,84</td>
            <td>Rp 36.967.760,84 - Rp 46.870.033,71</td>
            <td>Rp 46.870.033,72 - Rp 56.772.306,59</td>
            <td>Rp 56.772.306,59 - Rp 66.674.579,46</td>
            <td>>_ Rp 66.674.579,46</td>
            <td>1</td>
        </tr>
        <tr>
            <td><label for=""></label>PE2</td>
            <td>Rp 17.163.215,1 - Rp 27.065.497,96</td>
            <td>Rp 27.065.497,96 - Rp 36.967.760,84</td>
            <td>Rp 36.967.760,84 - Rp 46.870.033,71</td>
            <td>Rp 46.870.033,72 - Rp 56.772.306,59</td>
            <td>Rp 56.772.306,59 - Rp 66.674.579,46</td>
            <td>>_ Rp 66.674.579,46</td>
            <td>1</td>
        </tr>
        <tr>
            <td><label for=""></label>PE3</td>
            <td>Rp 17.163.215,1 - Rp 27.065.497,96</td>
            <td>Rp 27.065.497,96 - Rp 36.967.760,84</td>
            <td>Rp 36.967.760,84 - Rp 46.870.033,71</td>
            <td>Rp 46.870.033,72 - Rp 56.772.306,59</td>
            <td>Rp 56.772.306,59 - Rp 66.674.579,46</td>
            <td>>_ Rp 66.674.579,46</td>
            <td>1</td>
        </tr>
        <tr>
            <td><label for=""></label>PE4</td>
            <td>Rp 17.163.215,1 - Rp 27.065.497,96</td>
            <td>Rp 27.065.497,96 - Rp 36.967.760,84</td>
            <td>Rp 36.967.760,84 - Rp 46.870.033,71</td>
            <td>Rp 46.870.033,72 - Rp 56.772.306,59</td>
            <td>Rp 56.772.306,59 - Rp 66.674.579,46</td>
            <td>>_ Rp 66.674.579,46</td>
            <td>1</td>
        </tr>
    </tbody>
</table>

<br>
<br>
<button type="button" class="btn btn-primary btn-lg col-md-2" style="float: right;">Next</button>
</div>

<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->