<!-- Begin Page Content -->
<div class="container-fluid">

<!-- Page Heading -->
<h2 class="h5 mb-4 text-gray-800">Ayam</h2>

<h2 class="h3 mbcol-sm-2 text-gray-800">Skala-peternak</h2>
<h2 class="h3 mb-4 text-gray-800 d-flex justify-content-center">Rata – rata biaya konsumsi listrik yang dihabiskan untuk 1 ekor ayam </h2>
<br>

<div class="table-responsive">
<table class="table">
<thead>
<tr>
<th scope="col"></th>
<th scope="col"></th>
<th scope="col"></th>
<th scope="col">A</th>
<th scope="col">B</th>
</tr>
</thead>
<tbody>
<tr>
<td>1</td>
<td>Sangat Buruk</td>
<td>X > Rp. 526.30</td>
<td><input type="radio" name="nilai-ahp" id="pilihan-ahp" value="option2"></td>
<td><input type="radio" name="nilai-skala" id="pilihan-ahp" value="option2"></td>
</tr>
<tr>
<td>2</td>
<td>Buruk</td>
<td>Rp 355.63-Rp 526.30

</td>
<td><input type="radio" name="nilai-ahp" id="pilihan-ahp" value="option2"></td>
<td><input type="radio" name="nilai-skala" id="pilihan-ahp" value="option2"></td>
</tr>
<tr>
<td>3</td>
<td>Cukup</td>
<td>Rp 184.96-Rp 355.83
</td>
<td><input type="radio" name="nilai-ahp" id="pilihan-ahp" value="option2"></td>
<td><input type="radio" name="nilai-skala" id="pilihan-ahp" value="option2"></td>
</tr>
<tr>
      <td>4</td>
<td>Baik</td>
<td>Rp 14.29-Rp 184.96</td>
<td><input type="radio" name="nilai-ahp" id="pilihan-ahp" value="option2"></td>
<td><input type="radio" name="nilai-skala" id="pilihan-ahp" value="option2"></td>
</tr>
<tr>
      <td>5</td>
<td>Sangat Baik</td>
<td>X≤ Rp 14.29</td>
<td><input type="radio" name="nilai-ahp" id="pilihan-ahp" value="option2"></td>
<td><input type="radio" name="nilai-skala" id="pilihan-ahp" value="option2"></td>
</tr>
</tbody>
</table>

</div>





<br>
<button type="button" class="btn btn-primary btn-lg col-md-2" style="float: right;">Next</button>
</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->