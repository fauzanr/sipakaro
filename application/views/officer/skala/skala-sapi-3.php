<!-- Begin Page Content -->
<div class="container-fluid">

<!-- Page Heading -->

<h1 class="h3 mb-4 text-gray-800" style="float: right;">Pengisian Kuesioner Skala</h1>
<h1 class="h3 mb-4 text-gray-800">Skala</h1>
<h1 class="h4 mb-4 text-gray-800">Skala Sapi - RPH</h1>


<form>
    <div class="form-group row">
        <label class="col-sm-3 col-form-label">Populasi Sapi Potong (ekor)</label>
        <div class="col-sm-5">
            <input class="form-control" placeholder="Isi disini">
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-3 col-form-label">Rata-rata harga jual sapi (Rp)</label>
        <div class="col-sm-5">
            <input class="form-control" placeholder="Isi disini">
        </div>
    </div>
    <div class="form-group row">
        <label class="col-sm-3 col-form-label">Jumlah RTP</label>
        <div class="col-sm-5">
            <input class="form-control" placeholder="Isi disini">
        </div>
    </div>
</form>

<br>
<br>
<button type="button" class="btn btn-primary btn-lg col-md-2" style="float: right;">Next</button>
</div>


<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->